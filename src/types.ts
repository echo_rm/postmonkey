export type JOSEHeader = {
  typ: 'JWT';
  alg: 'RS256';
  [custom: string]: string;
};

export type JwtPayload = {
  /** Issuer */
  iss: string;
  /** Subject */
  sub: string;
  /** Audience */
  aud: string;
  /** Issued at */
  iat: number;
  /** Expiration time */
  exp: number;
  /** Not before */
  nbf?: string;
  /** JWT ID */
  jti?: string;
  /** Custom payload definitions */
  [custom: string]: any;
};

export type JwtAdditionalClaims = {
  [custom: string]: any;
}

export type RawToken = {
  header: string;
  payload: string;
};

export type SignedToken = string;

/**
 * The ASAP data that is required to generate an ASAP token.
 */
export interface AsapData {
  /** The ASAP Key ID */
  kid: string;
  /** The ASAP issuer */
  issuer: string;
  /** The ASAP ID of the recieving service. */
  audience: string;
  /** The Expiry time, in seconds, of the token that will be generated */
  expiry: number;
  /** The RSA private key that should be used for this token. */
  privateKey: string;
};

export type RsaPrivateKey = {
  privateKey: string;
};

export type Secret = RsaPrivateKey;

export type Reviver = (this: any, key: string, value?: any) => any;
