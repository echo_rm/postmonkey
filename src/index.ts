import { JOSEHeader, JwtPayload, SignedToken, AsapData, JwtAdditionalClaims, Reviver } from './types';
import { generateUUID } from './uuid';
import 'jsrsasign';
import { KJUR } from 'jsrsasign';

interface PostmanJWT {
  signRawToken(header: JOSEHeader, payload: JwtPayload, secret: string): SignedToken;
};

/*
function generateRawToken(header: JOSEHeader, payload: JwtPayload): RawToken {
  return {
    header: base64url(dataToSource(header)),
    payload: base64url(dataToSource(payload))
  };
}

function dataToSource(inputData: JOSEHeader | JwtPayload) {
  return CryptoJS.enc.Utf8.parse(JSON.stringify(inputData));
}

function base64url(source: any) {
  // Encode in classical base64
  let encodedSource = CryptoJS.enc.Base64.stringify(source);

  // Remove padding equal characters
  encodedSource = encodedSource.replace(/=+$/, '');

  // Replace characters according to base64url specifications
  encodedSource = encodedSource.replace(/\+/g, '-');
  encodedSource = encodedSource.replace(/\//g, '_');

  return encodedSource;
}
*/

function signRawToken(header: JOSEHeader, payload: JwtPayload, secret: string): SignedToken {
  return KJUR.jws.JWS.sign(header.alg, header, JSON.stringify(payload), secret);
}

export const jwt: PostmanJWT = {
  // generateRawToken,
  signRawToken
}

interface PostmanASAP {
  generateAsapToken(asapData: AsapData, additionalClaims?: JwtAdditionalClaims): SignedToken;
  parseAdditionalClaims(input: string, reviver?: Reviver): JwtAdditionalClaims;
}

function getUnixTimeSeconds(): number {
  return Math.round(new Date().getTime() / 1000);
}

function generateAsapToken(asapData: AsapData, additionalClaims?: JwtAdditionalClaims): SignedToken {
  const header: JOSEHeader = {
    typ: 'JWT',
    alg: 'RS256',
    kid: asapData.kid
  };

  const now = getUnixTimeSeconds();

  const payload: JwtPayload = {
    iss: asapData.issuer,
    sub: asapData.issuer,
    aud: asapData.audience,
    jti: generateUUID(),
    iat: now,
    exp: now + asapData.expiry,
    ...additionalClaims
  };

  return jwt.signRawToken(header, payload, asapData.privateKey);
}

const defaultReviver: Reviver = (_, value) => value;

/**
 * Takes an input string in the format of either a JSON object or comma separated key=value pairs
 * e.g. key1=value1,key2=value2
 * 
 * The optional reviver function allows for additional processing to convert values as needed.
 * Custom reviver functions may return any value desired, such as obtaining values from postman
 * variables.
 * 
 * The reviver function is also compatible with the second parameter of `JSON.parse()`,
 * for when JSON input is provided.
 * 
 * @param input JSON object or comma-separated key=value input string
 * @param reviver Transforms the original parsed value into any desired value.
 */
function parseAdditionalClaims(input: string, reviver: Reviver = defaultReviver): JwtAdditionalClaims {
  if (input.trim().startsWith('{')) {
      return JSON.parse(input, reviver);
  }
  return input
    .trim()
    .split(',')
    .filter(pair => pair !== "")
    .map((param: string) => param.split(/=(.*)/).slice(0, 2) as [string, string?])
    .reduce((result: JwtAdditionalClaims, [key, value]) => {
      result[key] = reviver.call(result, key, value);
      return result;
    }, {});
}

/**
 * This module allows you to generate ASAP tokens for your postman collection.
 *
 * @see https://developer.atlassian.com/platform/asap/
 */
export const asap: PostmanASAP = {
  generateAsapToken,
  parseAdditionalClaims
};
